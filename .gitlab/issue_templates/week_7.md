<!-- Name this issue "Week 7 - Seek mutual purpose" -->
This week practice seeking mutual purpose in conversations.

- [ ] Agree to agree in 5 conversations (Commit to staying in dialogue until some agreement/consensus is made).
- [ ] Ask "why" in 5 conversations (avoid the how, but ask what others' goals/purposes are).
- [ ] Find the "and" in 5 conversations (combine your purposes to find a higher level goal).

Try to think about and practice these skills in conversations while they are happening, but if you notice you were unable to, take the time to think about these items either in preparation of the conversation, or in retrospective.

- [ ] At the end of the week, share what you've noticed about these conversations.