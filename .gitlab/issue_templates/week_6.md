<!-- Name this issue "Week 6 - Learn to look" -->
Spend this week being mindful of your and the others' emotions.

- [ ] Make note of your emotions during 5 conversations. It may be helpful to prepare for the conversation by thinking "I'm going to pay attention to how I'm feeling during this conversation" or "I'm going to check in with what I'm feeling every 1-2 minutes during this conversation".
- [ ] Make note of how others are reacting in conversations. Look for moves to silence or verbal violence.
- [ ] At the end of the week, share what you've noticed about these conversations.

The goal this week is to improve awareness, but not necessarily work on recovery. If you notice others' moving one direction or another, you can consider using contrasting or start with heart to bring them back.
