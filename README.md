# :speaking_head_in_silhouette: Crucial Conversations

This project is for self-tracking my progress with the Crucial Conversations training. The intent is to focus on practicing one skill each week to better engrain the skill with my everyday behavior.

## :calendar: Weekly issues

- Week 1: [Unbundle with CPR](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/1)
- Week 2: [Masting my stories](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/2)
- Week 3: [Start with heart](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/3)
- Week 4: [State my path](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/4)
- Week 5: [Make it safe](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/5)
- Week 6: [Learn to look](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/6)
- Week 7: [Seek mutual purpose](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/7)
- Week 8: [Explore others' path](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/8)
- Week 9: [Move to action](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/9)
- Beyond: [Putting it all together](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/10)
- Beyond: [Refreshing your skills](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations/-/issues/11)

## How to use these issues

Crucial conversations can be personal in nature, so do not share specific details on conversations your are logging that if read by the other people in the conversation would make them feel badly in any way. Leave off names and generalize or change specifics if adding details so noone might be able to decipher who is being discussed. If you do wish to track these details for your own notes, make the project and issues private so only your user can view them. 

Many of these skills may not come up on the week you are practicing them. Note if they don't so you can revisit them later. Also consider creating a practice conversation with someone you trust to practice the skill with. Priming is an example of a skill that may not come up as often and may need some outside practice.

## Additional notes

Many of these issues recommend identifying 5 discussions per week to practice these skills in. Depending on your week, it may be that you simply don't have that many conversations that apply. This is ok, the idea is to practice being more aware of your conversations, and practice some specific skills so they become second nature. The awareness and preparation can occur for any given conversation even if it's not crucial. Then when you do have a crucial conversation, you'll be more likely to remember and use the skills you've practiced.

-----

Created by @sabrams
